from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import TodoItem, TodoList
from .forms import TodoListForm, TodoItemForm
from django.urls import reverse

class TodoListView(ListView):
    model = TodoList
    template_name = 'todos/todo_list.html'
    context_object_name = 'todo_lists'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['todo_list'] = self.get_queryset().first()
        return context

class TodoListDetail(DetailView):
    model = TodoList
    template_name = 'todos/todo_list_detail.html'
    context_object_name = 'todolist'

class TodoListCreateView(CreateView):
    model = TodoList
    form_class = TodoListForm
    template_name = 'todos/todo_list_create.html'
    success_url = '/todos/'

class TodoListUpdateView(UpdateView):
    model = TodoList
    form_class = TodoListForm
    template_name = 'todos/todo_list_update.html'
    context_object_name = 'todolist'

    def get_success_url(self):
        return reverse('todos:todo_list_detail', kwargs={'pk': self.object.pk})

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = 'todos/todo_list_delete.html'
    success_url = '/todos/'

class TodoItemCreateView(CreateView):
    model = TodoItem
    form_class = TodoItemForm
    template_name = 'todos/todo_item_create.html'

    def form_valid(self, form):
        form.instance.list_id = self.kwargs['pk']
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('todos:todo_list_detail', kwargs={'pk': self.kwargs['pk']})
    
class TodoItemUpdateView(UpdateView):
    model = TodoItem
    form_class = TodoItemForm
    template_name = 'todos/todo_item_update.html'

    def get_success_url(self):
        return reverse('todos:todo_list_detail', kwargs={'pk': self.object.list.pk})
