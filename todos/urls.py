from django.urls import path, include
from .views import TodoListView, TodoListDetail, TodoListCreateView, TodoListUpdateView, TodoListDeleteView, TodoItemCreateView, TodoItemUpdateView
from .models import TodoList

app_name = 'todos'

urlpatterns = [
    path('', TodoListView.as_view(), name='todo_list_list'),
    path('create/', TodoListCreateView.as_view(), name='todo_list_create'),
    path('<int:pk>/', TodoListDetail.as_view(), name='todo_list_detail'),
    path('<int:pk>/edit/', TodoListUpdateView.as_view(), name='todo_list_update'),
    path('<int:pk>/delete/', TodoListDeleteView.as_view(), name='todo_list_delete'),
    path('items/create/<int:pk>/', TodoItemCreateView.as_view(), name='todo_item_create'),
    path('items/<int:pk>/edit/', TodoItemUpdateView.as_view(), name='todo_item_update'),
]
